# -*- coding: utf-8 -*-

from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys

import time
import unittest
import pywinauto
import HTMLTestRunner
import sys

#global
driver = webdriver.Chrome()
driver.get('https://partnerka.project30.pro/')
driver.maximize_window()
wait = WebDriverWait(driver, 280)

countIU = 2
countAZS = 2
countCP = 2

with open(r"C:\Users\MSSidorkin\Documents\Flash\upload_docs\variable.txt") as file:
    array = [row.strip() for row in file]


class Selenium1_test_Pilot(unittest.TestCase):
    def test001_Login(self):
        driver.find_element_by_name('login').send_keys('maxim.sidorkin@project30.pro')
        driver.find_element_by_name('password').send_keys('$doNheX8'+Keys.RETURN)
        time.sleep(2)
        print('Проходим процедуру авторизации')
        driver.find_element_by_class_name('PageHelpVideo')
        driver.find_element_by_xpath(
            "//div[@class='FmButtonClose__icon -wait-no FmButtonClose__icon--size-medium']").click()
        _ = wait.until(EC.element_to_be_clickable((By.XPATH, "//div[@class='FmButtonLabel__wrap']")))
        driver.find_element_by_xpath("//div[@class='FmButtonLabel__wrap']").click()
        _ = wait.until(EC.element_to_be_clickable((By.XPATH, "(//INPUT[@type='text'])[1]")))
        time.sleep(1)

    def test002_CorrectCreateRequest(self):
        driver.find_element_by_xpath("(//INPUT[@type='text'])[1]").send_keys(array[0]+Keys.ENTER)
        driver.find_element_by_xpath("(//INPUT[@type='text'])[2]").send_keys(array[1]+Keys.ENTER)
        driver.find_element_by_xpath("(//INPUT[@type='text'])[3]").send_keys(array[2]+Keys.ENTER)
        driver.find_element_by_xpath("(//INPUT[@type='text'])[5]").send_keys(array[3])
        driver.find_element_by_class_name('FmButtonNext__icon').click()
        # insert here "500 error" check
        print('Заполняем поля корректно, и переходим к разделу "Паспортные данне"')

    def test003_CorrectCreatePassportData(self):
        time.sleep(1)
        driver.find_element_by_xpath("(//INPUT[@type='text'])[1]").send_keys(array[5])  # серия и номер паспорта
        driver.find_element_by_xpath("(//INPUT[@type='text'])[2]").send_keys(array[7])  # дата выдачи
        driver.find_element_by_xpath("(//INPUT[@type='text'])[3]").send_keys(array[9])      # код подразделения
        driver.find_element_by_xpath("(//INPUT[@type='text'])[4]").send_keys(array[9])      # код подразделения
        driver.find_element_by_xpath("(//INPUT[@type='text'])[5]").send_keys(array[11])  # дата рождения
        driver.find_element_by_xpath("(//INPUT[@type='text'])[6]").click()                  # пол
        driver.find_element_by_xpath("//DIV[@class='text'][text()='Женский']").click()
        driver.find_element_by_xpath("(//INPUT[@type='text'])[7]").send_keys('г Санкт-Петербург, Огородный пер, д 4, кв 22')  # адрес проживания
        time.sleep(1)   # 3
        driver.find_element_by_xpath("(//INPUT[@type='text'])[7]").send_keys(Keys.ENTER)
        #driver.find_element_by_xpath("(//INPUT[@type='text'])[8]").click()
        time.sleep(1)
        driver.find_element_by_xpath("(//INPUT[@type='text'])[1]").send_keys(Keys.PAGE_DOWN)
        time.sleep(1)
        driver.find_element_by_class_name('FmButtonNext__icon').click()
        print(' Заполняем поля корректно, и переходим к разделу "Работа"')
        print(' Извлекаем номер заявки')
        # draw
        draw = driver.find_element_by_xpath("//*[text()[contains(.,'Заявка №')]]")
        print('Полное название - ', draw.text)
        _ = draw.text   #8/11
        num = _[8:11]
        print('Только номер - ', num)

    def test004_TryCatchModalWindow(self):
        _ = wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR, "div.FmButtonClose__icon.-wait-no.FmButtonClose__icon--size-medium")))
        try:
            driver.find_element_by_xpath("//DIV[@class='FmButtonClose__icon -wait-no FmButtonClose__icon--size-medium']").click()
            print('Модальное окно "Распечайте форму согласия на обработку персональных данных" появилось и было закрыто')
        except:
            print("Модального окна не появилось")
        time.sleep(1)

    def test005_CorrectCreateWork(self):
        time.sleep(1)
        driver.find_element_by_xpath("(//INPUT[@type='text'])[1]").send_keys(array[13]+Keys.ENTER)  # Форма занятости
        driver.find_element_by_xpath("(//INPUT[@type='text'])[2]").send_keys(array[15]+Keys.ENTER)            # Отрасль работодателя
        driver.find_element_by_xpath("(//INPUT[@type='text'])[3]").send_keys(array[17])                    # ИНН
        time.sleep(1)
        driver.find_element_by_xpath("(//INPUT[@type='text'])[3]").send_keys(Keys.ENTER)                    # ИНН
        driver.find_element_by_xpath("(//INPUT[@type='text'])[4]").send_keys(array[19])                # Офиц. номер телефона
        driver.find_element_by_xpath("(//INPUT[@type='text'])[5]").send_keys(array[21]+Keys.ENTER)               # Стаж в текущем месте
        driver.find_element_by_xpath("(//INPUT[@type='text'])[6]").send_keys(array[23]+Keys.ENTER)              # Квалификация
        driver.find_element_by_xpath("(//INPUT[@type='text'])[7]").send_keys(array[25])                       # Доход в месяц в руб.
        driver.find_element_by_xpath("(//INPUT[@type='text'])[8]").send_keys(array[27]+Keys.ENTER)          # Кем приходится клиенту
        driver.find_element_by_xpath("(//INPUT[@type='text'])[9]").send_keys(array[29])            # Имя и отчество контактного лица
        driver.find_element_by_xpath("(//INPUT[@type='text'])[10]").send_keys(array[31])               # Телефон контактоного лица
        driver.find_element_by_class_name('FmButtonNext__icon').click()
        print(' Заполняем поля корректно, и переходим к разделу "Дополнительная информация"')

    def test006_CorrectAddInfo(self):
        time.sleep(1)
        driver.find_element_by_xpath("(//INPUT[@type='text'])[1]").send_keys(array[33])     # Образование
        time.sleep(1)
        driver.find_element_by_xpath("(//INPUT[@type='text'])[1]").send_keys(Keys.ARROW_DOWN+Keys.ENTER)
        driver.find_element_by_xpath("(//INPUT[@type='text'])[2]").send_keys(array[35])  # Серия и номер в/у
        driver.find_element_by_xpath("(//INPUT[@type='text'])[3]").send_keys(array[37])    # Дата выдачи в/у
        driver.find_element_by_xpath("(//INPUT[@type='text'])[4]").send_keys(array[39])          # Семейный статус
        time.sleep(1)
        driver.find_element_by_xpath("(//INPUT[@type='text'])[4]").send_keys(Keys.ENTER)
        driver.find_element_by_xpath("(//INPUT[@type='text'])[5]").send_keys('0')           # Количество лиц на иждивении
        driver.find_element_by_class_name('FmButtonNext__icon').click()
        print(' Заполняем поля корректно, и переходим к разделу "Параметры кредита и ТС"')

    def test007_CorrectCreateCredit(self):
        time.sleep(1)
        driver.find_element_by_xpath("(//INPUT[@type='text'])[1]").send_keys(array[41])   # Стоимость ТС, руб.
        driver.find_element_by_xpath("(//INPUT[@type='text'])[2]").send_keys(array[43])    # Первоначальный взнос, руб.
        driver.find_element_by_xpath("(//INPUT[@type='text'])[3]").send_keys(array[45])      # Срок кредита, мес.
        driver.find_element_by_xpath("(//INPUT[@type='text'])[4]").send_keys(array[47])    # Комфортный платёж, руб.
        #driver.find_element_by_class_name('FmButtonNext__icon').click()
        time.sleep(1)
        # Информация об автосалоне и ТС
        driver.find_element_by_xpath("(//INPUT[@type='text'])[5]").click()
        driver.find_element_by_xpath("(//INPUT[@type='text'])[5]").send_keys(array[49])
        time.sleep(1)
        driver.find_element_by_xpath("(//INPUT[@type='text'])[5]").send_keys(Keys.ENTER)
        time.sleep(1)
        driver.find_element_by_xpath("(//INPUT[@type='text'])[6]").click()
        driver.find_element_by_xpath("(//INPUT[@type='text'])[6]").send_keys(array[51]+Keys.ENTER)      # Б/У
        driver.find_element_by_xpath("(//INPUT[@type='text'])[7]").send_keys(array[53])        # Серия и номер ПТС
        driver.find_element_by_xpath("(//INPUT[@type='text'])[8]").send_keys(array[55])   # VIN автомобиля
        driver.find_element_by_xpath("(//INPUT[@type='text'])[9]").send_keys(array[57]+Keys.ENTER)          # Марка
        driver.find_element_by_xpath("(//INPUT[@type='text'])[10]").send_keys(array[59]+Keys.ENTER)            # Модель
        time.sleep(1)
        driver.find_element_by_class_name('FmButtonNext__icon').click()
        print(' Заполняем поля корректно, и переходим к разделу "Сбор документов"')

    def test008_UploadDocs(self):
        # загружаем документы
        time.sleep(1)
        driver.find_element_by_xpath("(//INPUT[@type='file'])[1]").send_keys(r"C:\Users\MSSidorkin\Documents\Flash\upload_docs\фото.jpg")
        print("Загружено фото пользователя")
        time.sleep(2)
        driver.find_element_by_xpath("(//INPUT[@type='file'])[2]").send_keys(r'C:\Users\MSSidorkin\Documents\Flash\upload_docs\пас1.jpg')
        print("Загружен скан паспорта №1")
        time.sleep(2)
        driver.find_element_by_xpath("(//INPUT[@type='file'])[2]").send_keys(r'C:\Users\MSSidorkin\Documents\Flash\upload_docs\пас2.jpg')
        print("Загружен скан паспорта №2")
        time.sleep(2)
        driver.find_element_by_xpath("(//INPUT[@type='file'])[2]").send_keys(r'C:\Users\MSSidorkin\Documents\Flash\upload_docs\пас3.jpg')
        print("Загружен скан паспорта №3")
        time.sleep(2)
        driver.find_element_by_xpath("(//INPUT[@type='file'])[2]").send_keys(r'C:\Users\MSSidorkin\Documents\Flash\upload_docs\пас4.jpg')
        print("Загружен скан паспорта №4")
        time.sleep(2)
        driver.find_element_by_xpath("(//INPUT[@type='file'])[2]").send_keys(r'C:\Users\MSSidorkin\Documents\Flash\upload_docs\пас5.jpg')
        print("Загружен скан паспорта №5")
        time.sleep(2)
        driver.find_element_by_xpath("(//INPUT[@type='file'])[4]").send_keys(r'C:\Users\MSSidorkin\Documents\Flash\upload_docs\согл.jpg')
        print("Загружено согласие на обработку персональных данных")
        time.sleep(2)
        driver.find_element_by_xpath("(//INPUT[@type='file'])[5]").send_keys(r'C:\Users\MSSidorkin\Documents\Flash\upload_docs\птс1.jpg')
        print("Загружен ПТС")
        time.sleep(2)
        driver.find_element_by_xpath("(//INPUT[@type='file'])[3]").send_keys(r'C:\Users\MSSidorkin\Documents\Flash\upload_docs\ву1.jpg')
        print("Загружено ВУ")
        time.sleep(2)
        # draw
        print('Извлекаем номер заявки')
        draw = driver.find_element_by_xpath("//*[text()[contains(.,'Заявка №')]]").text
        global num
        num = draw[8:12]
        print(num)
# отправляем заявку в банк
        wait.until(EC.visibility_of_element_located((By.XPATH,
                                                     "//*[text()[contains(.,'Набор обязательных документов загружен. "
                                                     "Можно отправлять на проверку в банк.')]]")))
        driver.find_element_by_tag_name('body').send_keys(Keys.PAGE_DOWN)
        time.sleep(1)
        driver.find_element_by_xpath("//DIV[@class='FmButtonNext__wrap'][text()='Отправить заявку в банк']").click()

    def test009_Verification(self):
        #self.skipTest(self)
        time.sleep(1)
        print("Переходим в Верификацию")
        driver.execute_script("window.open('https://verification-staging.project30.pro/admin/','_blank');")
        driver.switch_to.window(driver.window_handles[-1])

    def test010_LetMeIn(self):
        #self.skipTest(self)
        driver.find_element_by_id('username').send_keys('maxim.sidorkin')
        driver.find_element_by_id('password').send_keys('Moji78vixteR' + Keys.RETURN)
        _ = wait.until(EC.element_to_be_clickable((By.XPATH, "//SPAN[text()='Очередь']")))
        print("Авторизация в Верификации прошла успешно")

    def test011_Passport(self):
        #self.skipTest(self)
        try:
            driver.find_element_by_name('query').send_keys(num+'00' + Keys.RETURN)
            time.sleep(3)
            string = driver.find_element_by_xpath("//*[text()[contains(.,'Взять себе')]]")
            print(string.text)
        except:
            print('Element not found')
        driver.find_element_by_xpath("//*[text()[contains(.,'Взять себе')]]").click()
        time.sleep(1)
        driver.switch_to.window(driver.window_handles[-1])
        time.sleep(1)
        # первый скан
        driver.find_element_by_id('INPUT_PASSPORT_SERIES_NUMBER').send_keys(array[5])
        try:
            driver.find_element_by_id('correspondsToDocumentType--true').click()                        # Скан является страницей паспорта
            driver.find_element_by_id('scanIsWellReadableAndHasNoDefects--true').click()                # Хорошо читается, дефектов сканирования нет
            driver.find_element_by_id('noExtraNotes--true').click()                                     # Лишние отметки отсутствуют
            driver.find_element_by_id('theSeriesAndNumberAreTheSameOnAllPages--true').click()           # Серия и номер совпадают
            driver.find_element_by_id('theHueOfAllPagesIsTheSame--true').click()                        # Оттенок всех страниц одинаков
            driver.find_element_by_id('theInscriptionsDidNotBurnOutAndWereNotSmeared--true').click()    # Надписи не выгорели и не смазались
            print('Найдены элементы подтверждения ("галки") ПЕРВЫЙ СКАН')
        except:
            print("Элементы не найдены")
        time.sleep(1)

        # второй скан
        driver.find_element_by_xpath("(//DIV[@class='thumbnail__image'])[2]").click()
        time.sleep(1)
        try:
            driver.find_element_by_id('correspondsToDocumentType--true').click()                        # Скан является страницей паспорта
            driver.find_element_by_id('scanIsWellReadableAndHasNoDefects--true').click()                # Хорошо читается, дефектов сканирования нет
            driver.find_element_by_id('noExtraNotes--true').click()                                     # Лишние отметки отсутствуют
            driver.find_element_by_id('theSeriesAndNumberAreTheSameOnAllPages--true').click()           # Серия и номер совпадают
            print('Найдены элементы подтверждения ("галки") ВТОРОЙ СКАН')
            driver.find_element_by_xpath("//LABEL[@class='CheckBox__label']").click()                   # Отметить как разворот с фото
            time.sleep(0.5)
            driver.find_element_by_id('thereIsASignatureOfTheOfficialAndTheOwner--true').click()        # Есть подпись должностного лица и владельца
            driver.find_element_by_id('photoMeetsTheQualityRequirements--true').click()                 # Фотография соотв. треб. качества
            print('ВТОРОЙ СКАН отмечен как разворот страницы')
        except:
            print("Элементы не найдены")
        time.sleep(1)

        # третий скан
        driver.find_element_by_xpath("(//DIV[@class='thumbnail__image'])[3]").click()
        time.sleep(1)
        try:
            driver.find_element_by_id('correspondsToDocumentType--true').click()                        # Скан является страницей паспорта
            driver.find_element_by_id('scanIsWellReadableAndHasNoDefects--true').click()                # Хорошо читается, дефектов сканирования нет
            driver.find_element_by_id('noExtraNotes--true').click()                                     # Лишние отметки отсутствуют
            driver.find_element_by_id('theSeriesAndNumberAreTheSameOnAllPages--true').click()           # Серия и номер совпадают
            print('Найдены элементы подтверждения ("галки") ТРЕТИЙ СКАН')
        except:
            print("Элементы не найдены")
        time.sleep(1)

        # четвертый скан
        driver.find_element_by_xpath("(//DIV[@class='thumbnail__image'])[4]").click()
        time.sleep(1)
        try:
            driver.find_element_by_id('correspondsToDocumentType--true').click()                        # Скан является страницей паспорта
            driver.find_element_by_id('scanIsWellReadableAndHasNoDefects--true').click()                # Хорошо читается, дефектов сканирования нет
            driver.find_element_by_id('noExtraNotes--true').click()                                     # Лишние отметки отсутствуют
            driver.find_element_by_id('theSeriesAndNumberAreTheSameOnAllPages--true').click()           # Серия и номер совпадают
            print('Найдены элементы подтверждения ("галки") ЧЕТВЁРТЫЙ СКАН')
        except:
            print("Элементы не найдены")
        time.sleep(1)

        # пятый скан
        driver.find_element_by_xpath("(//DIV[@class='thumbnail__image'])[5]").click()
        time.sleep(1)
        try:
            driver.find_element_by_id('correspondsToDocumentType--true').click()                        # Скан является страницей паспорта
            driver.find_element_by_id('scanIsWellReadableAndHasNoDefects--true').click()                # Хорошо читается, дефектов сканирования нет
            driver.find_element_by_id('noExtraNotes--true').click()                                     # Лишние отметки отсутствуют
            driver.find_element_by_id('theSeriesAndNumberAreTheSameOnAllPages--true').click()           # Серия и номер совпадают
            print('Найдены элементы подтверждения ("галки") ПЯТЫЙ СКАН')
        except:
            print("Элементы не найдены")
        time.sleep(1)

    def test012_InputData(self):
        #self.skipTest(self)
        print('Переходим к заполнению паспортных данных')
        time.sleep(1)
        _ = wait.until(EC.element_to_be_clickable((By.ID, 'issuedBy')))
        driver.find_element_by_id('issuedBy').send_keys(array[61])             # Кем выдан
        print("Заполняем кем выдан паспорт")
        driver.find_element_by_id('issuedAt').send_keys(array[7])                                       # Дата выдачи
        print("Заполняем дату выдачи")
        driver.find_element_by_id('divisionCode').send_keys(array[9])                                      # Код подразделения
        print("Заполняем код подразделения")
        driver.find_element_by_id('lastName').send_keys(array[0])                                          # Фамилия
        print("Вводим фамилию")
        driver.find_element_by_id('firstName').send_keys(array[1])                                           # Имя
        print("Вводим имя")
        driver.find_element_by_id('secondName').send_keys(array[2])                                     # Отчество
        print("Вводим отчество")
        driver.find_element_by_id('birthday').send_keys(array[11])                                       # Дата рождения
        time.sleep(1)
        driver.find_element_by_id('birthday').send_keys(Keys.ENTER)
        print("Вводим дату рождения")
        driver.find_element_by_id('registrationAddress').send_keys(array[63])             # Ардес прописки
        time.sleep(1)
        driver.find_element_by_id('registrationAddress').send_keys(Keys.ENTER)
        print("Заполняем адрес проживания")
        driver.find_element_by_xpath("//DIV[@class='Button__content']").click()
        print("Паспортные данные внесены, подтверждаем паспорт (ГОТОВО)")

        try:
            time.sleep(2)
            driver.find_element_by_xpath("//DIV[@class='Wait__message-text'][text()='Все документы проверены']")
            print('Все документы проверены (ПАСПОРТ)')
        except:
            print('ОШИБКА!')

        driver.close()
        time.sleep(0.5)
        driver.switch_to.window(driver.window_handles[-1])

    def test013_InputConsent(self):
        #self.skipTest(self)
        time.sleep(1)
        driver.find_element_by_name('query').clear()
        time.sleep(1)
        driver.find_element_by_name('query').send_keys(num+'01' + Keys.RETURN)
        time.sleep(0.5)
        driver.find_element_by_xpath("//*[text()[contains(.,'Взять себе')]]").click()
        time.sleep(1)
        driver.switch_to.window(driver.window_handles[-1])
        time.sleep(0.5)
        driver.find_element_by_id('correspondsToExpectedType--true').click()
        driver.find_element_by_id('wellReadableAndHasNoDefects--true').click()
        driver.find_element_by_id('corporateTemplateUsed--true').click()
        driver.find_element_by_id('thereIsAPersonalDataProcessingConsentSignature--true').click()
        driver.find_element_by_id('thereIsACreditHistoryRequestSignature--true').click()
        time.sleep(0.5)
        driver.find_element_by_id('INPUT_PASSPORT_SERIES_NUMBER').send_keys(array[5])
        time.sleep(0.5)
        driver.find_element_by_xpath("//DIV[@class='Button__content']").click()

        try:
            time.sleep(2)
            driver.find_element_by_xpath("//DIV[@class='Wait__message-text'][text()='Все документы проверены']")
            print('Все документы проверены (CОГЛАСИЕ)')
        except:
            print('ОШИБКА!')

        driver.close()
        time.sleep(0.5)
        driver.switch_to.window(driver.window_handles[-1])

    def test014_InputDL(self):
        #self.skipTest(self)
        time.sleep(0.5)
        driver.find_element_by_name('query').clear()
        time.sleep(1)
        driver.find_element_by_name('query').send_keys(num+'03' + Keys.RETURN)
        time.sleep(1)

        #####
        try:
            e = driver.find_element_by_xpath("//*[text()[contains(.,'Ничего не найдено')]]")
            print('Found', e)
        except:
            print(' Not found')
        while driver.find_elements_by_xpath("//*[text()[contains(.,'Ничего не найдено')]]"):
            time.sleep(1)
            driver.find_element_by_name('query').send_keys(Keys.RETURN)
        else:
            driver.find_element_by_xpath("//*[text()[contains(.,'Взять себе')]]").click()
            print('Нашел и вышел из цикла')
        #####


        #driver.find_element_by_xpath("//*[text()[contains(.,'Взять себе')]]").click()
        time.sleep(1)
        driver.switch_to.window(driver.window_handles[-1])
        time.sleep(0.5)
        driver.find_element_by_id('correspondsToExpectedType--true').click()                            # Документ является в/у
        driver.find_element_by_id('wellReadableAndHasNoDefects--true').click()                          # Хорошо читается, дефектов скан. нет
        driver.find_element_by_xpath("//LABEL[@class='CheckBox__label']").click()                       # Отметить как скан с фото
        driver.find_element_by_id('issuedAt').send_keys(array[37])                                   # Дата выдачи
        driver.find_element_by_id('INPUT_DRIVER_LICENSE_SERIES_NUMBER').send_keys(array[35])         # серия и номер ВУ
        time.sleep(0.5)
        driver.find_element_by_xpath("//SPAN[@class='Button__label'][text()='Готово']").click()

        try:
            time.sleep(2)
            driver.find_element_by_xpath("//DIV[@class='Wait__message-text'][text()='Все документы проверены']")
            print('Все документы проверены (ВУ)')
        except:
            print('ОШИБКА!')

        driver.close()
        time.sleep(0.5)
        driver.switch_to.window(driver.window_handles[-1])

    def test015_InputPTS(self):
        #self.skipTest(self)
        time.sleep(0.5)
        driver.find_element_by_name('query').clear()
        time.sleep(1)
        driver.find_element_by_name('query').send_keys(num+'04' + Keys.RETURN)
        time.sleep(1)

        #####
        try:
            e = driver.find_element_by_xpath("//*[text()[contains(.,'Ничего не найдено')]]")
            print('Found', e)
        except:
            print(' Not found')
        while driver.find_elements_by_xpath("//*[text()[contains(.,'Ничего не найдено')]]"):
            time.sleep(1)
            driver.find_element_by_name('query').send_keys(Keys.RETURN)
        else:
            driver.find_element_by_xpath("//*[text()[contains(.,'Взять себе')]]").click()
            print('Нашел и вышел из цикла')
            #####

        #driver.find_element_by_xpath("//*[text()[contains(.,'Взять себе')]]").click()
        time.sleep(1)
        driver.switch_to.window(driver.window_handles[-1])
        time.sleep(0.5)
        driver.find_element_by_id('correspondsToExpectedType--true').click()                        # Документ является ПТС
        driver.find_element_by_id('wellReadableAndHasNoDefects--true').click()                      # Хорошо читается, дефектов скан. нет
        driver.find_element_by_id('inputVehiclePassportSeriesNumber').send_keys(array[53])     # Серия и номер ПТС
        driver.find_element_by_id('vin').send_keys(array[55])                             # VIN
        driver.find_element_by_id('brand').send_keys(array[57])                                         # Марка
        driver.find_element_by_id('model').send_keys(array[59])                                        # Модель
        driver.find_element_by_id('year').send_keys('2017')                                         # Год выпуска
        driver.find_element_by_id('enginePower').send_keys(array[69])                                 # Мощность
        driver.find_element_by_id('engineCapacity').send_keys(array[67])                               # Объем двигателя, см³
        driver.find_element_by_id('engineType--0').click()                                          # Тип двигателя

        driver.find_element_by_xpath("(//DIV[@class='RadioButton__check'])[2]").click()
        driver.find_element_by_xpath("(//DIV[@class='RadioButton__check'])[4]").click()

        time.sleep(1)
        driver.find_element_by_xpath("//SPAN[@class='Button__label'][text()='Готово']").click()

        try:
            time.sleep(2)
            driver.find_element_by_xpath("//DIV[@class='Wait__message-text'][text()='Все документы проверены']")
            print('Все документы проверены (ПТС)')
        except:
            print('ОШИБКА!')

        driver.close()
        time.sleep(0.5)
        driver.switch_to.window(driver.window_handles[-1])

    def test016_Photo(self):
        #self.skipTest(self)
        # сюда поставить ожидание фото (долго шло)
        time.sleep(0.5)
        driver.find_element_by_name('query').clear()
        time.sleep(1)
        driver.find_element_by_name('query').send_keys(num+'05' + Keys.RETURN)
        time.sleep(2)
        #####
        try:
            e = driver.find_element_by_xpath("//*[text()[contains(.,'Ничего не найдено')]]")
            print('Found', e)
        except:
            print(' Not found')
        while driver.find_elements_by_xpath("//*[text()[contains(.,'Ничего не найдено')]]"):
            time.sleep(1)
            driver.find_element_by_name('query').send_keys(Keys.RETURN)
        else:
            driver.find_element_by_xpath("//*[text()[contains(.,'Взять себе')]]").click()
            print('Нашел и вышел из цикла')
        #####
        time.sleep(2)
        driver.switch_to.window(driver.window_handles[-1])
        time.sleep(0.5)
        driver.find_element_by_id('left-scan--true').click()
        driver.find_element_by_id('right-scan--true').click()
        time.sleep(1)
        driver.find_element_by_xpath("//SPAN[@class='Button__label'][text()='Готово']").click()

        try:
            time.sleep(2)
            driver.find_element_by_xpath("//DIV[@class='Wait__message-text'][text()='Все документы проверены']")
            print('Все документы проверены (ФОТО)')
        except:
            print('ОШИБКА!')

        driver.close()
        time.sleep(0.5)
        driver.switch_to.window(driver.window_handles[-1])

    def test017_BackToRequest(self):
        #self.skipTest(self)
        driver.close()
        time.sleep(0.5)
        driver.switch_to.window(driver.window_handles[-1])
        time.sleep(1)
        #self.skipTest(self)
        wait.until(EC.element_to_be_clickable((By.XPATH, "//DIV[@class='FmButtonLabel__wrap']")))
        # описание элементов страницы
        print('Ожидание окончание телефонной верификации...')
        wait.until(EC.element_to_be_clickable((By.CLASS_NAME, "FmButtonLabel__wrap")))
        driver.find_element_by_xpath("(//DIV[@class='FmButtonRadio__icon -disabled-no -checked-no -focus-no'])[1]").click()
        driver.find_element_by_class_name('FmButtonNext__wrap').click()
        print('Переходим в раздел 7. Выбор условий, выбираем оно из условий и нажимаем ДАЛЕЕ >')

    def test018_Payment(self):
        #self.skipTest(self)
        wait.until(EC.element_to_be_clickable((By.XPATH, "(//INPUT[@type='text'])[1]")))
        driver.find_element_by_xpath("(//INPUT[@type='text'])[1]").send_keys('951357258719')
        driver.find_element_by_class_name('FmButtonNext__wrap').click()
        print('Вводим расчётный счёт и переходим к п. 9. Сделка')

    def test019_UploadDocs(self):
        self.skipTest(self)
        wait.until(EC.element_to_be_clickable((By.XPATH, "//A[@class='ButtonLoginPhone PageRequestStep09__loginPhone']")))
        # 1 Договор купли-продажи ТС
        # print('Ожидание окончание телефонной верификации...')
        driver.find_element_by_xpath("(//INPUT[@type='file'])[1]").click()
        time.sleep(1.5)
        app = pywinauto.application.Application()
        app.connect(title='Открытие')
        app.Dialog.Edit0.TypeKeys(r'C:\Users\MSSidorkin\Documents\Flash\upload_docs\ДКП.jpg', with_spaces=False)
        app.Dialog.Edit0.TypeKeys('{ENTER}')
        time.sleep(2)
        print('Загружен "Договор купли-продажи ТС"')
        # 2 Счёт на оплату ТС
        driver.find_element_by_xpath("(//INPUT[@type='file'])[2]").click()
        time.sleep(1.5)
        app = pywinauto.application.Application()
        app.connect(title='Открытие')
        app.Dialog.Edit0.TypeKeys(r'C:\Users\MSSidorkin\Documents\Flash\upload_docs\СчётОТС.jpg', with_spaces=False)
        app.Dialog.Edit0.TypeKeys('{ENTER}')
        time.sleep(2)
        print('Загружен "Квитанция об оплате ПВ"')
        # 3 Квитанция об оплате ПВ
        driver.find_element_by_xpath("(//INPUT[@type='file'])[3]").click()
        time.sleep(1.5)
        app = pywinauto.application.Application()
        app.connect(title='Открытие')
        app.Dialog.Edit0.TypeKeys(r'C:\Users\MSSidorkin\Documents\Flash\upload_docs\КвитОплПВ.jpg', with_spaces=False)
        app.Dialog.Edit0.TypeKeys('{ENTER}')
        time.sleep(2)
        print('Загружен "Счёт на оплату ТС"')
        # 4 Индивидуальные условия
        c = countIU - 1
        while c > 0:
            driver.find_element_by_xpath("(//INPUT[@type='file'])[4]").click()
            time.sleep(2.5)
            app = pywinauto.application.Application()
            app.connect(title='Открытие')
            app.Dialog.Edit0.TypeKeys(r'C:\Users\MSSidorkin\Documents\Flash\upload_docs\ИУ.pdf', with_spaces=False)
            app.Dialog.Edit0.TypeKeys('{ENTER}')
            time.sleep(2)
            print('Загружен "Индивидуальные условия" №', c)
            c = c - 1
        # 5 Анкета-заявка и согласия
        c = countAZS - 1
        while c > 0:
            driver.find_element_by_xpath("(//INPUT[@type='file'])[5]").click()
            time.sleep(1.5)
            app = pywinauto.application.Application()
            app.connect(title='Открытие')
            app.Dialog.Edit0.TypeKeys(r'C:\Users\MSSidorkin\Documents\Flash\upload_docs\Анкета.pdf', with_spaces=False)
            app.Dialog.Edit0.TypeKeys('{ENTER}')
            time.sleep(2)
            print('Загружен "Анкета-заявка и согласия №"', c)
            c = c - 1
        # 6 График платежей
        driver.find_element_by_xpath("(//INPUT[@type='file'])[6]").click()
        time.sleep(1.5)
        app = pywinauto.application.Application()
        app.connect(title='Открытие')
        app.Dialog.Edit0.TypeKeys(r'C:\Users\MSSidorkin\Documents\Flash\upload_docs\ГрП.pdf', with_spaces=False)
        app.Dialog.Edit0.TypeKeys('{ENTER}')
        time.sleep(2)
        print('Загружен "График платежей"')
        # 7 Карточка подписей
        c = countCP - 1
        while c > 0:
            driver.find_element_by_xpath("(//INPUT[@type='file'])[7]").click()
            time.sleep(1.5)
            app = pywinauto.application.Application()
            app.connect(title='Открытие')
            app.Dialog.Edit0.TypeKeys(r'C:\Users\MSSidorkin\Documents\Flash\upload_docs\КОП.pdf', with_spaces=False)
            app.Dialog.Edit0.TypeKeys('{ENTER}')
            time.sleep(2)
            print('Загружен "Карточка подписей" №', c)
            c = c - 1
        # 8 Заявление на открытие счетов
        driver.find_element_by_xpath("(//INPUT[@type='file'])[8]").click()
        time.sleep(1.5)
        app = pywinauto.application.Application()
        app.connect(title='Открытие')
        app.Dialog.Edit0.TypeKeys(r'C:\Users\MSSidorkin\Documents\Flash\upload_docs\ЗаявлНаОткрСчетов.pdf', with_spaces=False)
        app.Dialog.Edit0.TypeKeys('{ENTER}')
        time.sleep(2)
        print('Загружен "Заявление на открытие счетов"')
        # 9 Заявление на перевод денег за ТС
        driver.find_element_by_xpath("(//INPUT[@type='file'])[9]").click()
        time.sleep(1.5)
        app = pywinauto.application.Application()
        app.connect(title='Открытие')
        app.Dialog.Edit0.TypeKeys(r'C:\Users\MSSidorkin\Documents\Flash\upload_docs\ЗаявлНаПерев.pdf', with_spaces=False)
        app.Dialog.Edit0.TypeKeys('{ENTER}')
        time.sleep(2)
        print('Загружен "Заявление на перевод денег за ТС"')


if __name__ == '__main__':
    suite = unittest.TestSuite()
    suite.addTest(unittest.makeSuite(Selenium1_test_Pilot))

    buf = open("at_for_blank_Barmina.html", 'wb')
    runner = HTMLTestRunner.HTMLTestRunner(
        stream=buf,
        title='ПРОВЕРКА СОСТАВЛЕНИЯ ЗАВЯКИ И ВЕРИФИКАЦИИ ДЛЯ БАРМИНОЙ (нет услуг страхования)',
        description='Отчет по тестированию'
    )
    ret = not runner.run(suite).wasSuccessful()
    sys.exit(ret)